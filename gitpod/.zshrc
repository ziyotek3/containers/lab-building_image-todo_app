# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set the theme you want to use
ZSH_THEME="robbyrussell"

# Enable the plugins you want
plugins=(
  colored-man-pages
  colorize
  docker
  git
  kubectl
  zsh-autosuggestions
  zsh-syntax-highlighting
)

# GO Task Setup
mkdir -p ~/.oh-my-zsh/completions
fpath=(~/.oh-my-zsh/completions $fpath)
autoload -Uz compinit && compinit

source $ZSH/oh-my-zsh.sh

unalias gp

# You can add other configurations below this comment
